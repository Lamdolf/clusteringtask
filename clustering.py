import pandas as pd
import numpy as np
from scipy.cluster.hierarchy import dendrogram
import matplotlib.pyplot as plt
from math import sqrt
from pandas.core.frame import DataFrame
from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
import plotter


# Just what it says, get the data from file or internet
def getData():

    data = None
    try:
        data = pd.read_csv("./dataset.csv")

    except IOError:

        try:
            url = "https://gitlab.com/ALobhos/clusteringtask/-/raw/main/dataset.csv"
            data = pd.read_csv(url, header=None)
        except:
            print("No se ha podido encontrar el set de datos, revise su conexion")

    return data

# Save resulting csvs (for plotting)
def saveCSV(data, new, name):

    data["Cluster"] = new

    try:
        data.to_csv(f"./results/{name}")

    except IOError:
        print(f"No se ha podido guardar resultados de {name}")


# https://scikit-learn.org/stable/auto_examples/cluster/plot_agglomerative_dendrogram.html
def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_,
                                      counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)


# Printing some análisis of the data
def describeData(data):

    print(f"Filas y columnas: {data.shape}")
    print("Descripción de columnas")
    print(data.head)
    data.info()
    print(data.describe())
    

# Clean the data
def cleanData(data):

    cols = [x for x in data.columns]

    # Get the values of every row into an array
    newData = data.values
    
    # Drop the first two columns which are identifiers and indexes
    newData = newData[:, 2:]

    # All in range from 0 to 1
    normalData = normalize(newData)

    # Turn the numpy array back into a dataframe
    processedData = DataFrame(normalData)
    processedData.columns = cols[2:]

    return processedData

# Save a CSV in table format of distances from centroids
def saveCentroids(centroids):

    n_centroids = len(centroids)
    labels = [[f"C{i+1}"] for i in range(n_centroids)]
    cols = [f"C{i+1}" for i in range(n_centroids)]

    distances = pd.concat([DataFrame(labels), DataFrame(centroids)], axis=1)
    distances.columns = ["Centroid"] + cols

    distances.to_csv(f"./results/CentroidDistances_K{n_centroids}.csv")


# Calculate euclidian distance between centroids
def euclidianDistance(centroids):
    
    distances = []
    for x in centroids:
        aux = []
        for y in centroids:
            total = 0
            for i in range(5):
                total += (x[i]-y[i])**2
            aux.append(sqrt(total))
        distances.append(aux)

    return distances


# Cluste by K-means methods
def kMeans(data):
    
    # For testing the optima number of clusters
    inertias = [] 

    # Testing for 1 to 10 clusters
    for k in range(1,11):
        kmns = KMeans(n_clusters=k)
        kmns.fit(data)
        DataFrame(kmns.cluster_centers_).to_csv(f"./results/Centroids_K{k}.csv")
        distances = euclidianDistance(kmns.cluster_centers_)
        print(f"Distances for k = {k}: {kmns.inertia_}")
        inertias.append([k, kmns.inertia_])
        saveCSV(data, kmns.labels_, f"KMeans_K{k}.csv")
        saveCentroids(distances)
    
    # Saving the distances of al points to the clusters for plotting
    globalDistance = DataFrame(inertias)
    globalDistance.columns = ["K", "Distances"]
    globalDistance.to_csv("./results/Kmeans_Distances.csv")
    

# Clustering by hierarchic cluster
def hierarchicCluster(data):

    linkages = ["single", "average", "complete"]

    for link in linkages:

        # Computing the whole tree
        cluster = AgglomerativeClustering(n_clusters=None,
                                          linkage=link,
                                          compute_distances=True,
                                          distance_threshold=0)
        cluster.fit(data)
        saveCSV(data, cluster.labels_, f"Hierarchical_{link}.csv")
        if link != "single":
            plt.figure()
        
        # Generating the dendrograms
        plot_dendrogram(cluster, truncate_mode='level', p=3)
        plt.title(f"Dendrogram with {link} linkage")
        plt.savefig(f"./graphics/Dendrogram_{link}")


def main():

    # Initial data
    rawData = getData()
    describeData(rawData)

    # Clean data
    readyData = cleanData(rawData)
    describeData(readyData)

    # Clustering
    kMeans(readyData)
    hierarchicCluster(readyData)

    # Plotting
    plotter.initialPlots()
    plotter.generateClusters()
    plotter.distancePlot()
    

if __name__ == "__main__":
    main()
