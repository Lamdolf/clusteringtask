import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def generateClusters():

    for i in range(1,11):

        data = pd.read_csv(f"./results/KMeans_K{i}.csv")
        centroids = pd.read_csv(f"./results/Centroids_K{i}.csv")
        plt.figure()

        fig, axes = plt.subplots(2, 2, figsize=(15, 10), sharex=True)
        fig.suptitle(f"Clustering using K-Means with k={i}")

        sns.scatterplot(ax=axes[0,0], x="Avg_Credit_Limit",
                        y="Total_Credit_Cards", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[0,0], x="0", y="1",
                        data=centroids, marker="X", s=100, color="0.2")

        sns.scatterplot(ax=axes[0,1], x="Avg_Credit_Limit",
                        y="Total_visits_bank", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[0,1], x="0", y="2",
                        data=centroids, marker="X", s=100, color="0.2")

        sns.scatterplot(ax=axes[1,0], x="Avg_Credit_Limit",
                        y="Total_visits_online", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[1,0], x="0", y="3",
                        data=centroids, marker="X", s=100, color="0.2")

        sns.scatterplot(ax=axes[1,1], x="Avg_Credit_Limit",
                        y="Total_calls_made", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[1,1], x="0", y="4",
                        data=centroids, marker="X", s=100, color="0.2")

        plt.savefig(f"./graphics/Clustering_K{i}.png")
        plt.close()

    for i in ["single", "average", "complete"]:

        plt.figure()

        data = pd.read_csv(f"./results/Hierarchical_{i}.csv")

        fig, axes = plt.subplots(2, 2, figsize=(15, 10), sharex=True)
        fig.suptitle(f"Hierarchical Clustering with {i} linkage")

        sns.scatterplot(ax=axes[0,0], x="Avg_Credit_Limit",
                        y="Total_Credit_Cards", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[0,1], x="Avg_Credit_Limit",
                        y="Total_visits_bank", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[1,0], x="Avg_Credit_Limit",
                        y="Total_visits_online", data=data,
                        hue="Cluster", palette="deep")
        sns.scatterplot(ax=axes[1,1], x="Avg_Credit_Limit",
                        y="Total_calls_made", data=data,
                        hue="Cluster", palette="deep")

        plt.savefig(f"./graphics/Hierarchical_{i}.png")
        plt.close()


def initialPlots():
    
    data = pd.read_csv("./dataset.csv")

    fig, axes = plt.subplots(2, 2, figsize=(10, 10), sharex=True)
    fig.suptitle("Distribuciones de atributos vs Limite medio")

    sns.scatterplot(ax=axes[0,0], x="Avg_Credit_Limit", y="Total_Credit_Cards", data=data)
    sns.scatterplot(ax=axes[0,1], x="Avg_Credit_Limit", y="Total_visits_bank", data=data)
    sns.scatterplot(ax=axes[1,0], x="Avg_Credit_Limit", y="Total_visits_online", data=data)
    sns.scatterplot(ax=axes[1,1], x="Avg_Credit_Limit", y="Total_calls_made", data=data)
    plt.savefig("./graphics/Initial_Distribution.png")

def distancePlot():

    plt.figure()
    data = pd.read_csv("./results/Kmeans_Distances.csv")
    sns.lineplot(x="K", y="Distances", data=data)
    plt.title("Distance from points to centroid")
    plt.savefig("./graphics/Kmeans_Distances.png")

if __name__ == "__main__":
    # initialPlots()
    # generateClusters()
    distancePlot()

